//menu
const hamburger = document.querySelector(".header__button");
const menu = document.querySelector(".header__navigation");
const header = document.querySelector(".header");
const body = document.querySelector("body");
hamburger.addEventListener("click", function () {
  menu.classList.toggle("header__navigation--open");
  header.classList.toggle("header--open-navigation")
  body.classList.toggle('bodyOverflow');
});